import React from "react";
import { Dimensions, Animated } from "react-native";
import { Container, Input, BoxButtonSearch, SearchIcon } from "./styles";

export default function SearchBar() {
  const animation = new Animated.Value(50);
  const { width } = Dimensions.get("window");

  function onSearch() {
    Animated.spring(animation, {
      toValue: width * 0.6,
      useNativeDriver: false,
    }).start();
  }

  return (
    <Container style={{ width: animation }}>
      <Input autoFocus />
      <BoxButtonSearch onPress={onSearch}>
        <SearchIcon />
      </BoxButtonSearch>
    </Container>
  );
}
