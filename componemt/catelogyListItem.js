import React from "react";
import { Image, Text, View, TouchableOpacity } from "react-native";
import SkillImage from "../assets/favicon.png";
export default function CategoryListItem(props) {
  const { onPress } = props;
  return (
    <TouchableOpacity onPress={}>
      <View>
        <Text>Category 1</Text>
        <Image source={SkillImage} />
      </View>
      <View>
        <Text>Category 2</Text>
        <Image source={SkillImage} />
      </View>
      <View>
        <Text>Category 3</Text>
        <Image source={SkillImage} />
      </View>
    </TouchableOpacity>
  );
}
