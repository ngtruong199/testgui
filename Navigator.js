import React from 'react';
import {createStackNavigator} from 'react-navigation';

import category from './screen/category.js';
import categories from './screen/categories.js'; 
const Navigator = createStackNavigator({
    category : {
        screen: category
    },
    categories :{
        screen: categories
    }
});

export default Navigator;